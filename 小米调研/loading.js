(function (global) {
  (function insertGlobalLoading() {
    var loadingDomFragment = document.createDocumentFragment();

    // 添加loading根节点
    var loadingRoot = document.createElement('div');
    loadingRoot.setAttribute("id", "__page_loading")
    loadingRoot.style.cssText = 'position: fixed;top: 0;left: 0;width: 100%;height: 100%;display: block;z-index: 9999;background: rgba(255,255,255,0);text-align: center;';

    // 添加laoding的img节点
    var loadingImg = document.createElement('img');
    loadingImg.setAttribute('src', '/static/page_loading.svg');
    loadingImg.style.cssText = 'position: absolute;top: 50%;left: 50%;margin-top: -13px;margin-left: -13px;height: 26px;width: 26px;border-radius: 100%;display: inline-block;-webkit-animation-fill-mode: both;animation-fill-mode: both;-webkit-animation: loadingRotate 1.2s 0s linear infinite;animation: loadingRotate 1.2s 0s linear infinite;'
    
    // 添加loading的text节点，"加载中..."
    // 没有国际化翻译，暂时不显示
    // var loadingText = document.createElement('span');
    // loadingText.innerHTML = '加载中...'
    // loadingText.style.cssText = 'position: absolute;top: 50%;left: 50%;margin-top: 20px;margin-left: -20px;font-weight: 400;font-size: 14px;line-height: 1.5;color: #000;'
    // loadingRoot.appendChild(loadingText)
    
    // 添加loading的dom到loadingDomFragment中
    loadingRoot.appendChild(loadingImg)
    loadingDomFragment.appendChild(loadingRoot)

    // 把loadingDomFragment添加到body中
    document.documentElement.appendChild(loadingDomFragment)

    // 设置系统全局loading
    var __GlobalPageLoading__ = {
      loadQueue: [],
      show: function () {
        this.loadQueue.push(1)
        document.getElementById('__page_loading').style.display = 'block'
      },
      hide: function () {
        this.loadQueue.pop()
        if (this.loadQueue.length > 0) {
          return false
        }
        document.getElementById('__page_loading').style.display = 'none'
      },
      hideAll: function () {
        document.getElementById('__page_loading').style.display = 'none'
      },
    }
    if (!global.__GlobalPageLoading__) {
      global.__GlobalPageLoading__ = __GlobalPageLoading__
    }
  })();
})(typeof window === 'undefined' ? this : window);