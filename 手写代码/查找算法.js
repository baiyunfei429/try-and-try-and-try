// 二分查找
let binarySearch = (arr, target) => {
  let max = arr.length - 1;
  let min = 0;
  while(max >= min) {
    let middle = Math.floor((max + min) / 2);
    if(target < arr[middle]) {
      max = middle - 1;
    } else if(target > arr[middle]) {
      min = middle + 1;
    } else {
      return middle;
    }
  }
  return -1;
}